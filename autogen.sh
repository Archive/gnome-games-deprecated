#!/bin/sh
# Run this to generate all the initial makefiles, etc.

srcdir=`dirname $0`
test -z "$srcdir" && srcdir=.

PKG_NAME="Gnome Games Deprecated"

(test -f $srcdir/configure.in \
  && test -d $srcdir/gturing \
  && test -d $srcdir/scottfree) || {
    echo -n "**Error**: Directory "\`$srcdir\'" does not look like the"
    echo " top-level gnome directory"
    exit 1
}

#USE_COMMON_DOC_BUILD=yes
REQUIRED_AUTOMAKE_VERSION=1.9 
. gnome-autogen.sh
